CONFIGURATION_FILE = "conf/application.yaml"

license_definition_list = []


class Api:
    Proxy = "/APWeatherman/services"
    Login = Proxy + "/login"
    NewUser = Proxy + "/newUser"
    ChatUpload = Proxy + "/chatUpload"
    ChatRetrieve = Proxy + "/chatRetrieve"
    ChatList = Proxy + "/chatList"


class FlaskService:
    GET = "GET"
    POST = "POST"
    method_not_supported = "Method not supported!"
    service_api = "service_api"
    config_section = "FLASK"
    port = "port"


class Log:
    config_section = "LOG"


class Mongo:
    port = "port"
    host = "host"
    config_section = "MONGO"
    username = "username"
    password = "password"
    default_id = "_id"
    database_name = "AndhraPradeshWeatherman"
    login_collection = "LoginCollection"
    random_id_collection = "RandomIDCollection"
    user_role_collection = "UserRoleCollection"
    chat_collection = "ChatCollection"
