from scripts.constants.app_configuration import app_config
from scripts.constants.app_constants import Mongo
from scripts.logging import logger_util
from scripts.utilities.mongodb_utility import MongoDBUtility
from scripts.utilities.common_utils import CommonUtils

LOG = logger_util.get_logger()

mongo_obj = MongoDBUtility(mongo_host=app_config[Mongo.config_section][Mongo.host],
                           mongo_port=app_config[Mongo.config_section][Mongo.port])
common_obj = CommonUtils()


class Login(object):

    def __init__(self):
        """"""

    @staticmethod
    def user_login(input_data):
        final_json = {"status": False, "message": "login unsuccessful"}
        try:
            LOG.info("Inside Login Handler")
            login_user_details = mongo_obj.read(json_data={"userName": input_data["data"]["userName"]},
                                                database_name=Mongo.database_name,
                                                collection_name=Mongo.login_collection)
            if login_user_details:
                LOG.info("User Exists")
                actual_login_data = login_user_details[0]
                del actual_login_data["_id"]
                LOG.info("Validating the Login")
                if actual_login_data["password"] == input_data["data"]["password"]:
                    final_json["status"] = True
                    final_json["message"] = "Logged in Successfully"
                    final_json["data"] = actual_login_data
                    LOG.info("Login Successful")
                else:
                    LOG.info("Login Unsuccessful")
            else:
                LOG.info("User does not Exist")
                final_json["message"] = "User does not exist in the system"

        except Exception as e:
            LOG.error("Exception in Login")
            LOG.error(e)
        return final_json

    @staticmethod
    def create_new_user(input_data):
        final_json = {"status": False, "message": "User not Created"}
        try:
            LOG.info("Inside New User creation")
            new_user_details = mongo_obj.read(json_data={"userName": input_data["data"]["userName"]},
                                              database_name=Mongo.database_name,
                                              collection_name=Mongo.login_collection)
            if new_user_details:
                LOG.info("User Already Exists")
                final_json["message"] = "User already exists. Try with a new Username"

            else:
                LOG.info("User does not Exist in system. Can Proceed")
                input_data["data"]["userId"] = common_obj.unique_id_generator({"type": "user"})["unique_id"]
                input_data["data"]["userRoleID"] = mongo_obj.read(
                    json_data={"userRole": input_data["data"]["userRole"]}, database_name=Mongo.database_name,
                    collection_name=Mongo.user_role_collection)[0]["userRoleID"]

                mongo_obj.insert_one(json_data=input_data["data"], database_name=Mongo.database_name,
                                     collection_name=Mongo.login_collection)
                final_json["status"] = True
                final_json["message"] = "New user has been created"
                del input_data["data"]["_id"]
                final_json["data"] = input_data["data"]
                print(final_json)

        except Exception as e:
            LOG.error("Exception in Login")
            LOG.error(e)
        return final_json
