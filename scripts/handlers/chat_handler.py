from scripts.constants.app_configuration import app_config
from scripts.constants.app_constants import Mongo
from scripts.logging import logger_util
from scripts.utilities.mongodb_utility import MongoDBUtility
from scripts.utilities.common_utils import CommonUtils
from datetime import datetime

LOG = logger_util.get_logger()

mongo_obj = MongoDBUtility(mongo_host=app_config[Mongo.config_section][Mongo.host],
                           mongo_port=app_config[Mongo.config_section][Mongo.port])
common_obj = CommonUtils()


class Chat(object):

    def __init__(self):
        """"""

    @staticmethod
    def chat_upload(input_data):
        final_json = {"status": False, "message": "Chat Upload Unsuccessful"}
        try:

            user_role_for_user = mongo_obj.read(json_data={"userId": input_data["userId"]},
                                                database_name=Mongo.database_name,
                                                collection_name=Mongo.login_collection)[0]["userRoleID"]
            user_role_name = mongo_obj.read(json_data={"userRoleID": user_role_for_user},
                                            database_name=Mongo.database_name,
                                            collection_name=Mongo.user_role_collection)[0]["userRole"]
            if user_role_name != "Admin":
                existing_chat_data = mongo_obj.read(json_data={"userId": input_data["userId"]},
                                                    database_name=Mongo.database_name,
                                                    collection_name=Mongo.chat_collection)
            else:
                existing_chat_data = mongo_obj.read(json_data={"userId": input_data["data"]["to_user"]},
                                                    database_name=Mongo.database_name,
                                                    collection_name=Mongo.chat_collection)
            if existing_chat_data or user_role_name == "Admin":

                existing_chat_data = existing_chat_data[0]
                del existing_chat_data["_id"]
                existing_chat_data["chat_content"].append(input_data["data"])
                sorted_list = sorted(existing_chat_data["chat_content"],
                                     key=lambda x: datetime.strptime(x['timestamp'], '%Y-%m-%d %H:%M:%S'),
                                     reverse=False)
                existing_chat_data["chat_content"] = sorted_list
                if user_role_name != "Admin":
                    mongo_obj.update_one(json_data=existing_chat_data, condition={"userId": input_data["userId"]},
                                         collection_name=Mongo.chat_collection, database_name=Mongo.database_name)
                else:
                    mongo_obj.update_one(json_data=existing_chat_data,
                                         condition={"userId": input_data["data"]["to_user"]},
                                         collection_name=Mongo.chat_collection, database_name=Mongo.database_name)
                final_json["status"] = True
                final_json["message"] = "Chat Uploaded successfully"
            else:
                json_builder = dict()
                json_builder["userId"] = input_data["userId"]
                json_builder["chat_content"] = [input_data["data"]]
                mongo_obj.insert_one(json_data=json_builder, collection_name=Mongo.chat_collection,
                                     database_name=Mongo.database_name)
                final_json["status"] = True
                final_json["message"] = "Chat Uploaded successfully"
        except Exception as e:
            LOG.error("Exception in Login")
            LOG.error(e)
        return final_json

    @staticmethod
    def retrieve_chat(input_data):
        final_json = {"status": False, "message": "Chat Retrieve Unsuccessful"}
        try:
            existing_chat_data = mongo_obj.read(json_data={"userId": input_data["get_chat"]},
                                                database_name=Mongo.database_name,
                                                collection_name=Mongo.chat_collection)
            existing_chat_data = existing_chat_data[0]
            del existing_chat_data["_id"]
            final_json["data"] = existing_chat_data["chat_content"]
            final_json["status"] = True
            final_json["message"] = "Chats Retrieved successfully"
        except Exception as e:
            LOG.error("Exception in Login")
            LOG.error(e)
        return final_json

    @staticmethod
    def get_list_chats(input_data):
        final_json = {"status": False, "message": "Chat Retrieve Unsuccessful"}
        try:
            user_role_for_user = mongo_obj.read(json_data={"userId": input_data["userId"]},
                                                database_name=Mongo.database_name,
                                                collection_name=Mongo.login_collection)[0]["userRoleID"]
            user_role_name = mongo_obj.read(json_data={"userRoleID": user_role_for_user},
                                            database_name=Mongo.database_name,
                                            collection_name=Mongo.user_role_collection)[0]["userRole"]
            if user_role_name == "Admin":
                all_user_details = mongo_obj.read_without_inputjson(
                    database_name=Mongo.database_name,
                    collection_name=Mongo.chat_collection)
                user_list = []
                for each_user in all_user_details:
                    user = mongo_obj.read(json_data={"userId": each_user["userId"]},
                                                        database_name=Mongo.database_name,
                                                        collection_name=Mongo.login_collection)[0]
                    del user["_id"]

                    user_list.append(user)
                final_json["chat_group"] = user_list

            else:
                user = mongo_obj.read(json_data={"userId": "user_2"},
                                      database_name=Mongo.database_name,
                                      collection_name=Mongo.login_collection)[0]
                del user["_id"]
                final_json["chat_group"] = [user]

            final_json["status"] = True
            final_json["message"] = "Chats Retrieved successfully"
        except Exception as e:
            LOG.error("Exception in Login")
            LOG.error(e)
        return final_json
