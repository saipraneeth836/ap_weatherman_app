from scripts.constants.app_configuration import app_config
from scripts.constants.app_constants import Mongo
from scripts.logging import logger_util
from scripts.utilities.mongodb_utility import MongoDBUtility
import traceback

LOG = logger_util.get_logger()

mongo_obj = MongoDBUtility(mongo_host=app_config[Mongo.config_section][Mongo.host],
                           mongo_port=app_config[Mongo.config_section][Mongo.port])


class CommonUtils(object):
    def __init__(self):
        """

        """

    @staticmethod
    def unique_id_generator(input_data):
        final_json = {"unique_id": ""}
        try:
            unique_json = mongo_obj.read(json_data={"type": input_data["type"]},
                                         collection_name=Mongo.random_id_collection,
                                         database_name=Mongo.database_name)
            actual_unique_json = unique_json[0]
            del actual_unique_json["_id"]
            number_count = int(actual_unique_json["number_count"])
            number_count = number_count + 1
            actual_unique_json["number_count"] = number_count
            mongo_obj.update_one(condition={"type": input_data["type"]}, json_data=actual_unique_json,
                                 database_name=Mongo.database_name,
                                 collection_name=Mongo.random_id_collection)
            if input_data["type"] == "userRole":
                final_json["unique_id"] = "user_role_" + str(number_count)
            elif input_data["type"] == "user":
                final_json["unique_id"] = "user_" + str(number_count)
            elif input_data["type"] == "SupportTicket":
                final_json["unique_id"] = "support_ticket_" + str(number_count)
        except Exception as e:
            traceback.print_exc()
            LOG.exception(e)
            return {"status": "failed", "message": str(e)}
        return final_json
