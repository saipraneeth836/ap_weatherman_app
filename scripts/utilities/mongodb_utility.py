import json
import traceback

from pymongo import MongoClient

from scripts.constants.app_constants import Mongo
from scripts.logging import logger_util

LOG = logger_util.get_logger()
_mongo_conf_ = Mongo()


class MongoDBUtility(object):
    def __init__(self, mongo_host, mongo_port, user_name=None, password=None):
        try:
            self.__mongo_OBJ__ = MongoClient(
                host=mongo_host,
                port=mongo_port,
                username=user_name,
                password=password
            )

        except Exception as e:
            raise Exception(str(e))

    def insert_one(self, json_data, database_name, collection_name):
        try:
            mongo_response = self.__mongo_OBJ__[database_name][collection_name].insert_one(
                json_data)
            # mongo_response = self.fetch_records_from_object(mongo_response)
            return mongo_response.inserted_id
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def replace_one(self, json_data, database_name, collection_name):
        try:
            mongo_response = self.__mongo_OBJ__[database_name][collection_name].replace_one(
                json_data, "_id",
                upsert=False, )
            return mongo_response.inserted_id
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def insert_many(self, json_data, collection_name, database_name):
        try:
            mongo_response = self.__mongo_OBJ__[database_name][collection_name].insert_many(
                json_data)
            json_mongo_response_object = json.loads(json.dumps(mongo_response))
            return json_mongo_response_object
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def read(self, json_data, database_name, collection_name):
        try:
            db = self.__mongo_OBJ__[database_name]
            mongo_response = db[collection_name].find(json_data)
            mongo_response = self.fetch_records_from_object(mongo_response)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def read_without_inputjson(self, database_name, collection_name):
        try:
            db = self.__mongo_OBJ__[database_name]
            mongo_response = db[collection_name].find()
            mongo_response = self.fetch_records_from_object(mongo_response)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def find_with_limit(self, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].find(json_data)
            mongo_response1 = mongo_response.limit(1)
            mongo_response1 = self.fetch_records_from_object(mongo_response1)
            return mongo_response1
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def find_with_condition(self, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].find(json_data)
            mongo_response = self.fetch_records_from_object(mongo_response)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def remove(self, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            database_connection[collection_name].remove(json_data)
            return "success"
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def update_one(self, condition, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            database_connection[collection_name].update_one(condition, {"$set": json_data})
            return "success"
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def update(self, condition, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            database_connection[collection_name].update_one(condition, {"$set": json_data})
            return "success"
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def find_with_keyword(self, keyword, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].find({}, {keyword: 1})
            mongo_response = self.fetch_records_from_object(mongo_response)

            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def distinct_query(self, database_name, collection_name, query_key, filetry_json=None):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].distinct(query_key, filetry_json)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def find_with_multiple_keyword(self, keyword1, keyword2, keyword3, keyword4, keyword5,
                                   database_name,
                                   collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].find({},
                                                                       {keyword1: 1, keyword2: 1,
                                                                        keyword3: 1, keyword4: 1,
                                                                        keyword5: 1})
            mongo_response = self.fetch_records_from_object(mongo_response)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def aggregate_query(self, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].aggregate(json_data)
            mongo_response = self.fetch_records_from_object(mongo_response)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def delete_many(self, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].delete_many(json_data)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def get_doc_with_max_field_value(self, json_data, field_name, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            monogo_response = database_connection[collection_name].find(json_data).sort(
                [(field_name, -1)]).limit(1)
            return monogo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def skip_and_limit(self, database_name, collection_name, sort_field, skip_count, limit_count):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].find().sort(
                [(sort_field, -1)]).skip(
                skip_count).limit(limit_count)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def find_with_skip_and_limit_and_sort(self, json_data, database_name, collection_name,
                                          sort_field, skip_count,
                                          limit_count, sort):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            mongo_response = database_connection[collection_name].find(json_data).sort(sort_field,
                                                                                       sort).skip(
                skip_count).limit(limit_count)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    def close_connection(self):
        try:
            if self.__mongo_OBJ__ is not None:
                self.__mongo_OBJ__.close()
        except Exception as e:
            raise Exception(str(e))

    def list_database_name(self):
        database_list = self.__mongo_OBJ__.list_database_names()
        return database_list

    def list_collection_name(self, database_name):
        database = self.__mongo_OBJ__[database_name]
        collection_list = database.list_collection_names()
        return collection_list

    def create_db(self, database_name):
        database = self.__mongo_OBJ__[database_name]
        print(database)
        return database

    def create_collection(self, database_name, collection_name):
        database = self.__mongo_OBJ__[database_name]
        create_collection = database[collection_name]
        print(create_collection)
        return create_collection

    def find_one_with_condition(self, json_data, database_name, collection_name):
        try:
            database_connection = self.__mongo_OBJ__[database_name]

            mongo_response = database_connection[collection_name].find_one(json_data)
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))
    def search_record_by_query2(self, db_name, collection_name, query_json, search_option=None):
        """
        Definition for searching the record by query json
        :param search_option:
        :param db_name:
        :param collection_name:
        :param query_json:
        :return:
        """
        mg_response = {}
        try:
            response = {}
            docid = self.__mongo_OBJ__[db_name][collection_name]
            if query_json == {}:
                if search_option:
                    response = docid.find(query_json, search_option)
                else:
                    response = docid.find(query_json)
            else:
                if search_option:
                    response = docid.find(query_json, search_option)
                else:
                    # the below loop will take the last key value pair as an query json and it will give response
                    # for key, value in query_json.items():
                    #     response = docid.find({key: value})
                    response = docid.find(query_json)
            mg_response = self.fetch_records_from_object(response)
        except Exception as es:
            LOG.exception(es)
        return mg_response

    def cout_query(self, database_name, collection_name, query):
        try:
            database_connection = self.__mongo_OBJ__[database_name]
            # mongo_response = database_connection[collection_name].find(query).count()
            mongo_response = database_connection.get_collection(collection_name).find(query).count()
            return mongo_response
        except Exception as e:
            traceback.print_exc()
            raise Exception(str(e))

    @staticmethod
    def fetch_records_from_object(body):
        """

        :param body:
        :return:
        """
        final_list = []
        try:
            for doc in body:
                final_json = doc
                final_list.append(final_json)
        except Exception as e:
            status_message = "could not fetch records from object", str(e)
            LOG.error(status_message)
        return final_list

    def __del__(self):
        self.__mongo_OBJ__.close()
