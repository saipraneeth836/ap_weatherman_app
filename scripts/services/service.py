import json
import traceback

from flask import Blueprint, request

from scripts.constants.app_constants import FlaskService, Api
from scripts.handlers.login_handler import Login
from scripts.handlers.chat_handler import Chat
from scripts.logging import logger_util

LOG = logger_util.get_logger()

serv = Blueprint(FlaskService.service_api, __name__)


@serv.route(Api.Login, methods=['POST'])
def login():
    try:
        if request.method == FlaskService.POST:
            try:
                input_data = request.get_data()
                input_data = json.loads(input_data)
                response = Login().user_login(input_data)
                return response
            except Exception as e:
                LOG.exception(e)
                return {"status": "failed", "message": str(e)}
        else:
            return FlaskService.method_not_supported

    except Exception as e:
        traceback.print_exc()
        LOG.exception(e)
        return {"status": "failed", "message": str(e)}


@serv.route(Api.NewUser, methods=['POST'])
def new_user():
    try:
        if request.method == FlaskService.POST:
            try:
                input_data = request.get_data()
                input_data = json.loads(input_data)
                response = Login().create_new_user(input_data)
                return response
            except Exception as e:
                LOG.exception(e)
                return {"status": "failed", "message": str(e)}
        else:
            return FlaskService.method_not_supported

    except Exception as e:
        traceback.print_exc()
        LOG.exception(e)
        return {"status": "failed", "message": str(e)}


@serv.route(Api.ChatUpload, methods=['POST'])
def chat_enter():
    try:
        if request.method == FlaskService.POST:
            try:
                input_data = request.get_data()
                input_data = json.loads(input_data)
                response = Chat().chat_upload(input_data)
                return response
            except Exception as e:
                LOG.exception(e)
                return {"status": "failed", "message": str(e)}
        else:
            return FlaskService.method_not_supported

    except Exception as e:
        traceback.print_exc()
        LOG.exception(e)
        return {"status": "failed", "message": str(e)}


@serv.route(Api.ChatRetrieve, methods=['POST'])
def retrieve_chat():
    try:
        if request.method == FlaskService.POST:
            try:
                input_data = request.get_data()
                input_data = json.loads(input_data)
                response = Chat().retrieve_chat(input_data)
                return response
            except Exception as e:
                LOG.exception(e)
                return {"status": "failed", "message": str(e)}
        else:
            return FlaskService.method_not_supported

    except Exception as e:
        traceback.print_exc()
        LOG.exception(e)
        return {"status": "failed", "message": str(e)}

@serv.route(Api.ChatList, methods=['POST'])
def chat_list():
    try:
        if request.method == FlaskService.POST:
            try:
                input_data = request.get_data()
                input_data = json.loads(input_data)
                response = Chat().get_list_chats(input_data)
                return response
            except Exception as e:
                LOG.exception(e)
                return {"status": "failed", "message": str(e)}
        else:
            return FlaskService.method_not_supported

    except Exception as e:
        traceback.print_exc()
        LOG.exception(e)
        return {"status": "failed", "message": str(e)}
